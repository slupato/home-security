## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Requirements](#requirements)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

## Title
Home Security System

## Short Description
Home security system project using Jetson NANO

When Jetson detects a person it will take and send pictures to telegram on chosen time interval.

## Requirements

Jetson NANO, NANO compatible camera, PC/Laptop, VLC media player, Telegram

## Install

**jetson-inference**

Follow instuctions in this video https://www.youtube.com/watch?v=bcM5AQSAzUY to flash sd card with nvidia jetpak image and install jetson-inference, for this project we need resnet18-body.

**telegram-bot**

Get your telegram bot, bot token and chat id using these instructions https://github.com/python-telegram-bot/python-telegram-bot/wiki/Extensions-%E2%80%93-Your-first-Bot.


**VLC**


Install VLC https://www.videolan.org/vlc/

## Usage

**Create VLC stream file**

Create .sdp file on your PC/laptop with following content.
Replace PORT with the port of your choosing and IP with your Jetson IP.

```
v=0
m=video PORT RTP/AVP 96
c=IN IP4 IP
a=rtpmap:96 H264/90000
```

**Clone git repo to your Jetson and change folder**
```
git clone https://gitlab.com/slupato/home-security.git
cd homesec
```

**CONFIG**

Modify config.py file with your telegram bot token, chatid, IP and camera info.

**Run the program**

Jetson:
```
python3 homesec.py
```

PC(only for checking Jetson video feed):

Run the .sdp file you created



## Maintainer

Tuomas Uusi-Luomalahti @slupato

## Contributing

Tuomas Uusi-Luomalahti @slupato

## License

Free for non-commercial use
