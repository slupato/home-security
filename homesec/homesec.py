import jetson.inference
import jetson.utils
import telegram
import config

timer = int(0)
font = jetson.utils.cudaFont (size = 24)

# Create telegram bot
Guard = telegram.Bot(token=config.Token)
chatId = config.ChatID 


# Create video source, stream, poseNet and frame save
source = jetson.utils.videoSource(config.Camera, argv=['--input-width=1920', '--input-height=1080'])
stream = jetson.utils.videoOutput(config.StreamIP, argv=['--bitrate=10000000', '--headless'])
net = jetson.inference.poseNet("resnet18-body", threshold=0.40)
frame = jetson.utils.videoOutput("detect.jpg", argv=['--headless'])


Guard.send_message(text='Security system online', chat_id=chatId)


while True:
    imgNet = source.Capture()

    font.OverlayText(imgNet, imgNet.width, imgNet.height, "NEURAL NET FPS: {:.0f}".format(net.GetNetworkFPS()), 5, 5, (255,0,0),(0,0,0))

    stream.Render(imgNet)

    DetectPerson = net.Process(imgNet)

    if DetectPerson:
        if timer == 0:
            imgRaw = source.Capture()
            frame.Render(imgRaw)
            Guard.send_photo(chat_id=chatId, photo=open('detect.jpg', 'rb'))
            timer = int(net.GetNetworkFPS()) # Set timer to send pic every second

    
    if timer > 0:
        timer = timer - 1

    if not source.IsStreaming() or not stream.IsStreaming():
        Guard.send_message(text='Security system offline', chat_id=chatId)
        break